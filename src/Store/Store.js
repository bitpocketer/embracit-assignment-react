import {createStore,applyMiddleware,combineReducers} from 'redux';
import thunk from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension';
//import reducers and plugginto combine reducers for creating store
import ProfileReducer from '../Reducers/Profile.Reducer';

export default createStore(combineReducers({ProfileReducer}),{},composeWithDevTools(applyMiddleware(thunk)));


import React from 'react';
import {Link} from 'react-router-dom';
import Navigation from './Navigation/Navigations';
export default class firstComponent extends React.Component {
    render() {
        return (
            <div>
                <Navigation />
                <div className="Sidebar">
                    <div className="NavButtons">
                    <ul className="Nav-Buttons">
                        <li>
                            <Link to ="/"> Home</Link>
                        </li>
                        <li>
                            <Link to ="/Projects">Projects</Link>
                        </li>
                        <li>
                            <Link to ="/Products">Products</Link>
                        </li>
                        <li>
                            <Link to ="/Partners">Partners</Link>
                        </li>
                        <li>
                            <Link to ="/Aboutus">Aboutus</Link>
                        </li>
                    </ul>
                    </div>
                </div>
            </div>
        )
    }
}
import React from 'react';
import Navigation from './Navigation/Navigations';
import Footer from './Navigation/footer';
import Searchbar from './Searchbar/Searcbar';
export default class Home extends React.Component {
    render() {
        return (
            <div className="homeheader">
                {/* <Navigation/> */}
                {/* <Searchbar/> */}
                {/* <h1>This is home</h1> */}
                <div className="container">
                    <div className="header-nav">
                        <div className="header-left-nav">
                            <ul>
                                <li><a href="#aboutus">About us</a></li>
                                <li><a href="#ourvision">OUR Vision</a></li>
                                <li><a href="#ourwork">OUR Work</a></li>
                            </ul>
                        </div>
                        <div className="header-right-nav">
                            <ul>
                            <li><a href="#ourclients">Clients</a></li>
                            <li><a href="#ourmethodology">Methadology</a></li>
                            <li><a href="#contactus">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                    <div className="HeaderBody">
                        <div className="HeaderBodyContent">
                            <h1>Influence Human <span>Technologies</span></h1>
                            <p>Start the development with a BadAss Tutor Jawad</p>
                            <p> Jay Kit Inspired by Salis</p>
                        </div>
                        <div className="section" id="aboutus">
                            <div className="TagLine">
                                <p>We create beautiful websites, Salis is a leading web developer with
                                    a profound industrial experience geared up to push the limits of UI/UX Design
                                    We create beautiful websites, Salis is a leading web developer with
                                    a profound industrial experience geared up to push the limits of UI/UX Design
                                    We create beautiful websites, Salis is a leading web developer with
                                    a profound industrial experience geared up to push the limits of UI/UX Design
                                    We create beautiful websites, Salis is a leading web developer with
                                    a profound industrial experience geared up to push the limits of UI/UX Design
                                    We create beautiful websites, Salis is a leading web developer with
                                    a profound industrial experience geared up to push the limits of UI/UX Design
                                    We create beautiful websites, Salis is a leading web developer with
                                    a profound industrial experience geared up to push the limits of UI/UX Design
                                    </p>
                            </div>
                            <div className="ParentColumn">
                                <div className="Column">
                                    <div className="ColumnDescription">
                                        <img src={require("../Assets/logo.svg")}></img>
                                        <h3>Tech & Trade & Buisness</h3>
                                        <p>
                                            Every element that you need in a product comes built in as a component. All components fit perfectly with each other and can take variations in colour
                                            </p>
                                    </div>
                                </div>
                                <div className="Column">
                                    <div className="ColumnDescription">
                                        <img src={require("../Assets/logo.svg")}></img>
                                        <h3>Huge Number Of Components</h3>
                                        <p>
                                            Every element that you need in a product comes built in as a component. All components fit perfectly with each other and can take variations in colour
                                            </p>
                                    </div>
                                </div>
                                <div className="Column">
                                    <div className="ColumnDescription">
                                        <img src={require("../Assets/logo.svg")}></img>
                                        <h3>Professional Team</h3>
                                        <p>
                                            Every element that you need in a product comes built in as a component. All components fit perfectly with each other and can take variations in colour
                                            </p>
                                    </div>
                                </div>
                            </div>
                            <div className="sectionmiddle">
                                <div className="BasicComponents">
                                    <h3>Basic Component</h3>
                                    <h6>The Core elements of your website</h6>
                                    <p>
                                        We create beautiful websites, Salis is a leading web developer with
                                    a profound industrial experience geared up to push the limits of UI/UX Design
                                    We create beautiful websites, Salis is a leading web developer with
                                    </p>
                                </div>
                                <div className="sectionmiddle-image" >
                                    <img src={require("../Assets/table.jpeg")}></img>
                                </div>
                            </div>
                            <div className="sectioncards">
                                <div className="cardsimage">
                                    {/* <img src ={require("../Assets/webheader.jpg")}></img> */}
                                    <h3>Beauty is Turning Caffine Into Code</h3>

                                </div>
                                <div className="cardsdescription">
                                    <h4>Unconventional Cards</h4>
                                    <p>ONE CARD FOR EVERY PROBLEMWe love cards and everybody on the web seems to.
                                     We have gone above and beyond with options for you to organise your information.
                                     From cards designed for blog posts, to product cards or user profiles, you will
                                     have many options to choose from. All the cards follow the material principles and
                                    have a design that stands out.</p>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="contentarea" id="ourvision">
                        <div className="ContentAreasText">
                            <h4>Our Vision</h4>
                            <blockquote>
                                This is how we do it
                                </blockquote>
                            <p>
                                It is with great pride that I address our Zain family.

                            Zain has accomplished many achievements never before encountered in the industry. Our leading pace has always been accredited to our culture, with a deep understanding that in order to improve the lives of our clients we must reflect our internal working culture and our promise for a wonderful world.

                            Our teams evidently understand that how we operate is just as crucial as what we accomplish. It is through the spirit of teamwork and strong sense of vision that we stride towards our goals.

                            Historically we have witnessed many technological disruptions that have wiped out entire industries. Operational agility is most essential in order to keep up with the speed of change and need to pivot across all areas of business.

                            Although we are focused on our core services, today we must recognize that Zain has not identified itself as being limited as a telecoms company, but rather a digital service provider. One that enhances all areas of its clients lives adapting them to the ever-changing interconnectivity and digital lifestyle era.

                            Our clients come first; identifying their needs and wants is the driving force that pulls us towards our eagerness to deliver.

                            Hand in hand we take the journey to pioneer the way in this fast paced environment as we pave the way towards creating value for all our stakeholders and truly honor our vision towards a wonderful world.
                            It is with great pride that I address our Zain family.

                            Zain has accomplished many achievements never before encountered in the industry. Our leading pace has always been accredited to our culture, with a deep understanding that in order to improve the lives of our clients we must reflect our internal working culture and our promise for a wonderful world.

                            Our teams evidently understand that how we operate is just as crucial as what we accomplish. It is through the spirit of teamwork and strong sense of vision that we stride towards our goals.

                            Historically we have witnessed many technological disruptions that have wiped out entire industries. Operational agility is most essential in order to keep up with the speed of change and need to pivot across all areas of business.

                            Although we are focused on our core services, today we must recognize that Zain has not identified itself as being limited as a telecoms company, but rather a digital service provider. One that enhances all areas of its clients lives adapting them to the ever-changing interconnectivity and digital lifestyle era.

                            Our clients come first; identifying their needs and wants is the driving force that pulls us towards our eagerness to deliver.

                            Hand in hand we take the journey to pioneer the way in this fast paced environment as we pave the way towards creating value for all our stakeholders and truly honor our vision towards a wonderful world.
                            </p>
                        </div>
                        <div className="contentAreaImage">
                            <img src={require('../Assets/yay.jpg')} />
                        </div>
                    </div>
                    <div className="portfolio" id="ourwork">
                        <h3>Our Work</h3>
                        <div className="grid-container">
                            <div className="grid-item">
                                <img src={require('../Assets/logo.svg')} />
                            </div>
                            <div className="grid-item">
                                <img src={require('../Assets/logo.svg')} />
                            </div>
                            <div className="grid-item">
                                <img src={require('../Assets/logo.svg')} />
                            </div>
                            <div className="grid-item">
                                <img src={require('../Assets/logo.svg')} />
                            </div>
                            <div className="grid-item">
                                <img src={require('../Assets/logo.svg')} />
                            </div>
                            <div className="grid-item">
                                <img src={require('../Assets/logo.svg')} />
                            </div>
                            <div className="grid-item">
                                <img src={require('../Assets/logo.svg')} />
                            </div>
                            <div className="grid-item">
                                <img src={require('../Assets/logo.svg')} />
                            </div>
                            <div className="grid-item">
                                <img src={require('../Assets/logo.svg')} />


                            </div>
                        </div>
                    </div>
                    <div className="slogans" id="ourmethodology">
                        <h1>Your Work Will Get Much Easier</h1>
                        <div className="slogansgrid">
                            <div className="sloganitem">
                                <h3> Professional Team</h3>
                                <div class="sloganicon">
                                    <img src={require('../Assets/logo.svg')} />
                                </div>
                                <h3>Bootstrap Grid</h3>
                                <p>
                                    Enjoy the fluid grid system based on 12 columns.
                                 Material Kit PRO is built over Bootstrap and has
                                  all the benefits that the framework comes with.
                                </p>
                            </div>
                            <div className="sloganitem">
                                <h3> Fully Agile</h3>
                                <div class="sloganicon">
                                    <img src={require('../Assets/logo.svg')} />
                                </div>
                                <h3>Bootstrap Grid</h3>
                                <p>
                                    Enjoy the fluid grid system based on 12 columns.
                                 Material Kit PRO is built over Bootstrap and has
                                  all the benefits that the framework comes with.
                                </p>
                            </div>
                            <div className="sloganitem">
                                <h3> Cost Effective</h3>
                                <div class="sloganicon">
                                    <img src={require('../Assets/logo.svg')} />
                                </div>
                                <h3>Bootstrap Grid</h3>
                                <p>
                                    Enjoy the fluid grid system based on 12 columns.
                                 Material Kit PRO is built over Bootstrap and has
                                  all the benefits that the framework comes with.
                                </p>

                            </div>
                        </div>
                    </div>
                    <div className="clients" id="ourclients">
                        <h2>Trusted By 350.00O people</h2>
                        <p> These are some morons who trust us. </p>
                        <div className="clients-grid">
                            <div className="clientitem">
                                <div className="clientimage">
                                    <img src={require('../Assets/yay.jpg')} />
                                </div>
                                <p>
                                    "As soon as I saw this kit, everything else isn't the same anymore,
                                I just can't describe it guys! Thank you for this work!"
                                </p>
                            </div>
                            <div className="clientitem">
                                <div className="clientimage">
                                    <img src={require('../Assets/yay.jpg')} />
                                </div>
                                <p>
                                    "As soon as I saw this kit, everything else isn't the same anymore,
                                I just can't describe it guys! Thank you for this work!"
                                </p>
                            </div>
                            <div className="clientitem">
                                <div className="clientimage">
                                    <img src={require('../Assets/yay.jpg')} />
                                </div>
                                <p>
                                    "As soon as I saw this kit, everything else isn't the same anymore,
                                I just can't describe it guys! Thank you for this work!"
                                </p>
                            </div>
                        </div>
                        <div className="clientorganization">
                            <div className="clientorgnaziationitem">
                                <img src={require('../Assets/logo.svg')} />
                            </div>
                            <div className="clientorgnaziationitem">
                                <img src={require('../Assets/logo.svg')} />
                            </div>
                            <div className="clientorgnaziationitem">
                                <img src={require('../Assets/logo.svg')} />
                            </div>
                        </div>
                    </div>
                    <div className="contact-us"  id="contactus">
                        <div className="contact-form">
                            <div className="contactusmessage">
                                <p>Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            Feel free to get in touch
                            </p>
                            </div>
                            <div className="forms">
                            <h3> Get In Touch</h3>
                            <form className="conform">
                                <div className="controls">
                                    <label name="namelabel" htmlFor="Name" className="form-label">Name</label>
                                    <input name="Name" type="text" className="form-control"></input>
                                </div>
                                <div className="controls">
                                    <label name="emaillabel" htmlFor="Email" className="form-label" >Email</label>
                                    <input name="Email" type="email" className="form-control"></input>
                                </div>
                                <div className="controls">
                                    <label name="messagelabel" htmlFor="Name" className="form-label">Message</label>
                                    <textarea name="Message" type="richtext" className="form-control"></textarea>
                                </ div>
                                <button type="submit" className="button-control">Send</button>
                            </form>
                            </div>

                        </div>
                    </div>
                    <Footer />
                </div>
            </div>
        )
    }
}
import React from 'react';
import ProfilePic from './Components/ProfileImage';
import { connect } from 'react-redux';
import { fetchUserProfile, createprofile } from '../../Actions/Profile.Actions';
import $ from 'jquery';
import Navigation from '../Navigation/Navigations';
class Profile extends React.Component {
    componentDidMount() {
        //add event listener for displaying button for creation of a profile if profile doesn't exist
        window.addEventListener("displaycreatebutton", this.displaycreatebutton);
        window.addEventListener("userexists", this.displaydangerousmessage);
        window.addEventListener('hidedanger', this.hidedangermessage);
        window.addEventListener("pcc", this.confirmcreation)
        console.log('props on did mount', this.props);
        if (this.props.User.FirstName != null) {
            this.refs.name.value = this.props.User.FirstName + " " + (typeof this.props.User.LastName != 'undefined' || this.props.User.LastName != null) ? this.props.User.LastName : "";
            this.refs.description.value = this.props.User.Description;
            this.refs.location.value = this.props.User.Location;
            this.refs.namelabel.value = this.props.User.FirstName + " " + this.props.User.LastName;
        }
    }
    componentWillReceiveProps(props) {
        console.log('recieved props', props);
        this.refs.name.value = props.User.FirstName + " " + props.User.LastName;
        this.refs.description.value = props.User.Description;
        this.refs.location.value = props.User.Location;
        this.refs.namelabel.value = props.User.FirstName + " " + props.User.LastName;
    }

    render() {
        return (
            <div>
                <Navigation />
                <div className="Profile">

                    <ProfilePic />

                    <div className="form">
                        <div className="name">
                            <label ref="namelabel">Name</label>
                            <input type="text" size="53" ref="name" />
                        </div>
                        <span>
                            <label>Description</label>
                        </span>

                        <div className="DescriptionBox">
                            <textarea rows="4" cols="50" ref="description">
                            </textarea>

                        </div>
                        <div className="Location">
                            <label>Location</label>
                            <input type="text" size="51" ref="location" />
                        </div>
                        <div className="updatebutton">
                            <button id="updateprofilebutton" onClick={this.updateprofile.bind(this)}>Update profile</button>
                            <button id="createprofilebutton" onClick={this.createprofile.bind(this)}>Create profile</button>
                            <p id="dangermessage">User already exists create with another name or go to login page </p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    updateprofile() {
        var names = this.refs.name.value.split(" ");
        var UserData = {
            firstname: names[0],
            lastname: names[1],
            description: this.refs.description.value,
            location: this.refs.location.value
        }
        // console.log('user data',UserData);
        this.props.fetchUserProfile(UserData);
    }
    createprofile() {
        var names = this.refs.name.value.split(" ");
        console.log(names[0])

        var UserData = {
            firstname: names[0],
            lastname: names[1],
            description: this.refs.description.value,
            location: this.refs.location.value
        }
        this.props.createprofile(UserData);
    }
    displaycreatebutton() {
        console.log('display create button called');
        $("#createprofilebutton").show();
        $("#updateprofilebutton").css("display", "none");
    }
    displaydangerousmessage() {
        $('#dangermessage').show();
    }
    hidedangermessage() {
        $('#dangermessage').hide();
    }
    confirmprofilecreation() {
        console.log('creation event');
        $('#dangermessage').show();
        $('#dangermessage').text('profile created login with first name and last name on login page');
        $('#dangermessage').show();

    }
}
const mapStateToProps = state => {
    return {
        User: state.ProfileReducer.User
    }
}
export default connect(mapStateToProps, { fetchUserProfile, createprofile }, null, { withRef: true })(Profile)
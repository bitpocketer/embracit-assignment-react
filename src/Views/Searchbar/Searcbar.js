import React from 'react';
import SearchInput, {createFilter} from 'react-search-input'
const   mails=[{'name':'jawad','subject':'job'},{'name':'Hamza','subject':'Nothing'}
,{'name':'Qasim','subject':'Sick leave'},{'name':'Jawad','subject':'Home work'},{'name':'Faizan','subject':'Salery delayed'},{'name':'hayat','subject':'apply'}
,{'name':'Ehsan','subject':'Resgination'}
,{'name':'Soman','subject':'Promotion'}
,{'name':'Hamid','subject':'Discipline'}
,{'name':'Kashif','subject':'Release Date For BotAny'}
,{'name':'Umar','subject':'Leave For paper'}
,{'name':'Falak','subject':'Wedding Leave'}
,{'name':'yahya','subject':'Salery revision'}
,{'name':'Ayesha','subject':'B-ed Exam'}
,{'name':'faran','subject':`Doesn't Exist`}
,{'name':'salis','subject':'Increment Please'}];
const KEYS_TO_FILTERS = ['name', 'subject']

export default class Search extends React.Component{
    constructor(props){
        super(props);
        this.state={
            searchTerm:''
        }
        this.searchUpdated=this.searchUpdated.bind(this)
    }
    render(){
         const filteredMails=mails.filter(createFilter(this.state.searchTerm,KEYS_TO_FILTERS))
         return(
            <div className="Fuzzy-search">
            <SearchInput fuzzy={true} className="search-input" onChange={this.searchUpdated} />
            <div className="mails-parent">
            {filteredMails.map(email => {
              return (
                <div className="mail" key={email.name}>
                  <div className="from">{email.name}</div>
                  <div className="subject">{email.subject}</div>
                </div>
              )
            })}
          </div>
          </div>
         )
    }
    searchUpdated(term){
        this.setState({searchTerm:term});
    }   
    
}
import React from 'react';
import { Route,Redirect } from 'react-router'
import Navigation from '../Navigation/Navigations';
import {Signin} from '../../Actions/Profile.Actions';
import {connect} from 'react-redux'
 class Login extends React.Component {
     constructor(props){
         super();
         this.state={
             redirect:false
         }
     }
     componentDidMount(){
        //event listener for redirecting to profile on signing in successfully
         window.addEventListener('redirectprofile',this.setredirection.bind(this));
     }
     componentWillReceiveProps(props){
         if(props.userdata!==null){
            this.setState({redirect:true})
         }
     }
     RedirectRender=()=>{
         if(this.state.redirect){
             return <Redirect to="/Profile"/>
         }
     }
    render() {
        return (
            <div>
                {this.RedirectRender()}
                <Navigation/>
                <form id="login-form" onSubmit={this.Login.bind(this)}>
                    <label htmlFor="FirstName"> First Name</label>
                    <input type="text" id="FirstName" ref="fn" />
                    <label htmlFor="LastName" >Last Name</label>
                    <input type="text" id="LastName" ref="ln"/>
                    <button id="Login" type="submit">Log in</button>
                    <p> don't have id ? sign up</p>
                    <button id="signup">Sign up</button>
                </form>
            </div>
        )
    }
    Login(e){
        e.preventDefault();
        var UserData={
            firstname:this.refs.fn.value,
            lastname:this.refs.ln.value
        }
        this.props.Signin(UserData)
    }
    setredirection(){
        this.setState({redirect:true});
    }
}
const mapStateToProps=state=>{
    return{
        userdata:state.ProfileReducer.User
    }
}
export default connect(mapStateToProps, { Signin }, null, { withRef: true })(Login)
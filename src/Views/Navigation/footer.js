import React from 'react'
export default class Footer extends React.Component {
    render() {
        return (
            <footer className="footer">
                <div className="footercontainer">
                    <a className="footer-brand" href="">JayTech</a>
                    <ul className="pull-center-footer-list">
                        <li><a href="">Creative Tim</a></li>
                        <li><a href="#aboutus" >About Us</a></li>
                        <li><a href="#contactus">Contact Us</a></li>
                    </ul>
                <div className="social-media">
                    <ul className="pull-right-footer-social">
                        <li><img src={require('../../Assets/logo.svg')}/></li>
                    </ul>
                </div>
                </div>
            </footer>
        )
    }
}
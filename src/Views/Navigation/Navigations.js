import React from 'react';
import {Link} from 'react-router-dom';
import Searchbar from '../Searchbar/Searcbar';
export default class Navigation extends React.Component{
    render(){
        return(
            <ul className="Nav">
                <li>
                    <Link to ="/">Home </Link>
                </li>
                <li>
                    <Link to ="/Profile">Profile </Link>
                </li>

                <li>
                    <Link to ="/Login">Login </Link>
                </li>
                <li>
                    <Link to ="/FirstComponent">Products </Link>
                </li>
                <li>
                    <Link to ="/FirstComponent">Partners </Link>
                </li>
                <li>
                    <Link to ="/FirstComponent">About us </Link>
                </li>
                <li>
                    <Link to ="/FirstComponent">Contact us </Link>
                </li>


            </ul>
        )
    }
}
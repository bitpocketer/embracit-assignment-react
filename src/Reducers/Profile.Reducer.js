//import actions from actions
import { GETUSER, UPDATEUSER } from '../Actions/Profile.Actions';

const ProfileReducer = (state = { User: {} }, action) => {
    let newState = {};
    switch (action.type) {
        case GETUSER:
            state = action.data;
            newState = {
                User: action.data
            };
            return Object.assign({}, state, newState);
        case UPDATEUSER:
            state = action.data;
            return Object.assign({}, state, newState);
        default:
            return state;
    }
    // return state;

}
export default ProfileReducer;
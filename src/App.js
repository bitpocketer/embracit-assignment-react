import React, { Component } from 'react';
import logo from './Assets/logo.svg';
import './Assets/App.css';
import FirstComponent from './Views/FirstComponent';
import Home from './Views/Home';
import Profile from './Views/Profile/Profile';
import Login from './Views/Login/Login';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
class App extends Component {
  render() {
    return (
      <div>
        <Router>
          <Switch>
            <Route exact path={"/"} component={Home} />
            <Route exact path="/FirstComponent" component={FirstComponent} />
            <Route exact path="/Profile" component={Profile}/>
            <Route  path="/Login" component={Login}/>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;

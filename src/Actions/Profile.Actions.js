import $ from 'jquery'
export const GETUSER="GET_USER";
export const UPDATEUSER="UPDATE_USER";
var  Apiurl= require( '../Services/Apiendpoint');
export function fetchUserProfile(Credentials){
    return dispatch=>{
        $.ajax({
            method:'post',
            data:Credentials,
            url:Apiurl.Apiurl+'/updateuser',
            dataType:'json',

            success:function(UserData){
                console.log(UserData);
                if(UserData=='create user first'){
                    var myevent=new Event('displaycreatebutton');
                    window.dispatchEvent(myevent);
                }else{
                return dispatch(fetchuser(UserData))
            }
            },
            error:function(err){
                // console.log(err);
            }
        })
    }
}
function fetchuser(UserData){
    return{
        type:GETUSER,
        data:UserData
    }
}
export function Signin(Credentials){
    console.log('cred in signin',Credentials);
    return dispatch=>{
        $.ajax({
            method:'post',
            data:Credentials,
            url:Apiurl.Apiurl+'/login',
            dataType:'json',

            success:function(UserData){
                if(UserData=="usernotfound"){
                    var useralreadyexist=new Event('usernotfound');
                    window.dispatchEvent(useralreadyexist);
                }
                else{
                return dispatch(fetchuser(UserData))
                var Redirect=new Event('redirectprofile');
                window.dispatchEvent(Redirect);
                }
            },
            error:function(err){
                // console.log(err);
            }
        })
    }

}

export function createprofile(Credentials){
    return dispatch=>{
        $.ajax({
            method:'post',
            data:Credentials,
            url:Apiurl.Apiurl+'/createprofile',
            dataType:'json',

            success:function(UserData){
                if(UserData=="user already exists"){
                    var useralreadyexist=new Event('userexists');
                    window.dispatchEvent(useralreadyexist);
                }
                else{
                return dispatch(fetchuser(UserData))
                var confirmprofilecreation=new Event('pcc');
                window.dispatchEvent(confirmprofilecreation);
                }
            },
            error:function(err){
                // console.log(err);
            }
        })
    }

}
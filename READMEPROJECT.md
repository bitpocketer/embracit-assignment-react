App is built with the boiler plate CreateReact APP
To Inspect the change in states kindly use the chrome extension "Redux DevTools 2.15.2"
Actions folder has a file having definitions of Actions dispatched onto the store.
Reducers folder has a profile Reducer..
Services folder has url of the api endpoint 
Views folder has all the react components
Store folder has store file having definition for redux store
App.js file has the routes of the application. 

#instructions for running an app
1:- clone the project
2:- Install packages typing "npm install" in terminal
3:- type npm start in terminal to run the project
